/*
 see
 https://audioprograming.wordpress.com/2012/10/18/a-simple-synth-in-android-step-by-step-guide-using-the-java-sdk/
 http://michaelkrzyzaniak.com/AudioSynthesis/
 */

package com.example.utf1247.myapplication;

import android.app.Activity;
import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioTrack;
import android.os.Bundle;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.SeekBar;
import android.widget.Toast;

public class MainActivity extends Activity {

    CheckBox chkBoxSine;
    CheckBox chkBoxSquare;
    CheckBox chkBoxSawtooth;
    CheckBox chkBoxTriangle;
    short soundType = 0;
    short sinSnd = 0;
    short squSnd = 1;
    short sawSnd = 2;
    short triSnd = 3;

    Thread t;
    int sr = AudioTrack.getNativeOutputSampleRate(AudioManager.STREAM_MUSIC); // GN: maybe // 44100;
    boolean isRunning = true;

    SeekBar fSlider;
    double sliderval;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // point the slider to the GUI widget
        fSlider = (SeekBar) findViewById(R.id.frequency);

        // create a listener for the slider bar;
        SeekBar.OnSeekBarChangeListener listener = new SeekBar.OnSeekBarChangeListener() {
            public void onStopTrackingTouch(SeekBar seekBar) {
            }

            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (fromUser)
                    sliderval = progress / (double) seekBar.getMax();
            }
        };

        // set the listener on the slider
        fSlider.setOnSeekBarChangeListener(listener);

        initialUISetup();


        // start a new thread to synthesise audio
        t = new Thread() {
            public void run() {
                // set process priority
                setPriority(Thread.MAX_PRIORITY);

                int buffsize = AudioTrack.getMinBufferSize(sr, AudioFormat.CHANNEL_OUT_MONO,
                        AudioFormat.ENCODING_PCM_16BIT);
                // create an audiotrack object
                AudioTrack audioTrack = new AudioTrack(AudioManager.STREAM_MUSIC, sr,
                        AudioFormat.CHANNEL_OUT_MONO,
                        AudioFormat.ENCODING_PCM_16BIT,
                        buffsize,
                        AudioTrack.MODE_STREAM);

                short samples[] = new short[buffsize];
                int amp = 32767; // GN: ? // 10000;
                double twopi = 8. * Math.atan(1.);
                double fr = 440.f;
                double ph = 0.0;  // period = SAMPLE_RATE / frequency

                // start audio
                audioTrack.play();

                // synthesis loop
                while (isRunning) {

                    double halfWavelength = sr / (2 * fr);
                    double nextSampleSq = -1;
                    double nextSampleTri = -1;
                    double incrementAmmount = -2.0 / (halfWavelength + 1);

                    fr = 440 + 440 * sliderval;

                    // GN: I guess this ok (in 16 bit wav PCM, first byte is the low order byte - Stereo)
                    for (int i = 0; i < buffsize; i++) {

                        if (soundType == sinSnd) {
                            // p = a * sin(2 * %pi * freq * t); ... where 't=0:(1/44100):1;'
                            samples[i] = (short) (amp * Math.sin(ph));
                            ph += twopi * fr / sr;

                        } else if (soundType == sawSnd) {

                            // http://michaelkrzyzaniak.com/AudioSynthesis/2_Audio_Synthesis/1_Basic_Waveforms/5_Sawtooth_Wave/
                            // http://stackoverflow.com/questions/17604968/generate-sawtooth-tone-in-java-android
                            samples[i] = (short) (amp * (2 * (i % (sr / fr)) / (sr / fr) - 1));

                        } else if (soundType == squSnd) {

                            if (0 == (i % (int) halfWavelength)) {
                                nextSampleSq *= -1;
                            }
                            //audioBuffer[i] = nextSample;
                            samples[i] = (short) (amp * nextSampleSq);

                        } else if (soundType == triSnd) {

                            if (0 == (i % (int) halfWavelength)) {
                                incrementAmmount *= -1;
                            }
                            nextSampleTri += incrementAmmount;
                            //audioBuffer[i] = nextSample;
                            samples[i] = (short) (amp * nextSampleTri);
                        }

                    }
                    audioTrack.write(samples, 0, buffsize);
                }

                audioTrack.stop();
                audioTrack.release();
            }
        };

        t.start();
    }

    public void onDestroy() {
        super.onDestroy();
        isRunning = false;
        try {
            t.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        t = null;
    }

    /*
    http://www.technotalkative.com/android-checkbox-example/
     */
    public void initialUISetup() {
        chkBoxSine = (CheckBox) findViewById(R.id.chkSine);
        chkBoxSquare = (CheckBox) findViewById(R.id.chkSquare);
        chkBoxSawtooth = (CheckBox) findViewById(R.id.chkSawtooth);
        chkBoxTriangle = (CheckBox) findViewById(R.id.chkTriangle);

        chkBoxSine.setOnCheckedChangeListener(new myCheckBoxChnageClicker());
        chkBoxSquare.setOnCheckedChangeListener(new myCheckBoxChnageClicker());
        chkBoxSawtooth.setOnCheckedChangeListener(new myCheckBoxChnageClicker());
        chkBoxTriangle.setOnCheckedChangeListener(new myCheckBoxChnageClicker());
    }

    class myCheckBoxChnageClicker implements CheckBox.OnCheckedChangeListener {
        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            // TODO Auto-generated method stub

            // Toast.makeText(CheckBoxCheckedDemo.this, &quot;Checked =&gt; &quot;+isChecked, Toast.LENGTH_SHORT).show();

            if (isChecked) {
                if (buttonView == chkBoxSine) {
                    soundType = sinSnd;
                    showTextNotification("Sine");
                }
                if (buttonView == chkBoxSquare) {
                    soundType = squSnd;
                    showTextNotification("Sqare");
                }
                if (buttonView == chkBoxSawtooth) {
                    soundType = sawSnd;
                    showTextNotification("Sawtooth");
                }
                if (buttonView == chkBoxTriangle) {
                    soundType = triSnd;
                    showTextNotification("Triangle");
                }
            }
        }
    }

    public void showTextNotification(String msgToDisplay) {
        Toast.makeText(this, msgToDisplay, Toast.LENGTH_SHORT).show();
    }
}
